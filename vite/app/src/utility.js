
const projectComparator = (a, b) => {
  const nameA = a.fullName.toUpperCase()
  const nameB = b.fullName.toUpperCase()
  if (nameA < nameB) return -1
  if (nameA > nameB) return 1
  return 0
}

const flatten = arrayOfArrays => [].concat.apply([], arrayOfArrays)

const filterObjectByKeys = (object, keys) => Object.fromEntries(Object.entries(object).filter(([key]) => keys.includes(key)))

const difference = (a, b) => new Set([...a].filter(x => !b.has(x)))

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

export {
	projectComparator,
	flatten,
	filterObjectByKeys,
	difference,
	delay
}