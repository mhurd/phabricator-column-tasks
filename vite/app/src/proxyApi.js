import { flatten } from './utility.js'

const serverBaseURL = `${window.location.protocol}//${window.location.hostname}:3000`

const postOptions = () => {
    return {
        'method': 'POST',
        'headers': {
            'Content-Type': 'application/json; charset=utf-8', 
            'Accept': 'application/json'   
        }
    }
}

const postOptionsWithBody = body => {
    return {...postOptions(), body: JSON.stringify(body)}
}

const updateTaskOwner = async (userPHID, taskID) => {
    const response = await fetch(`${serverBaseURL}/task-assign`, postOptionsWithBody({userPHID, taskID}))
    return await response.json()
}

const fetchProjectsForProjectIDs = async (ids) => {
    const response = await fetch(`${serverBaseURL}/projects`, postOptionsWithBody({ids}))
    return await response.json()
}

const fetchColumnsForProjectPHIDs = async (phids) => {
    const response = await fetch(`${serverBaseURL}/project-columns`, postOptionsWithBody({phids}))
    return await response.json()
}

const fetchPropertiesForUserNames = async (userNames) => {
    const response = await fetch(`${serverBaseURL}/username-properties`, postOptionsWithBody({userNames}))
    return await response.json()
}

const fetchTasksForColumnAndProxyPHIDs = async (phids) => {
    const response = await fetch(`${serverBaseURL}/column-and-proxy-tasks`, postOptionsWithBody({phids}))
    return await response.json()
}

async function* fetchProjectsColumnsAndTasks(ids) {
    const projects = await fetchProjectsForProjectIDs(ids) 
    yield {'projects': projects}

    const projectPHIDs = projects.map(project => project.phid) 
    const columns = await fetchColumnsForProjectPHIDs(projectPHIDs) 
    yield {'columns': columns}
    yield {'finishedLoadingPHIDs': projectPHIDs}

    for (let i = 0; i < projects.length; i++) {
        const project = projects[i]
        // a column can either show its own tasks or tasks from another project if proxyPHID is specified
        const projColumns = columns[project.phid]
        const projectColumnAndProxyPHIDs = flatten(Object.values(projColumns)).map(column => (column.proxyPHID === null) ? column.phid : column.proxyPHID)
        const tasks = await fetchTasksForColumnAndProxyPHIDs(projectColumnAndProxyPHIDs) 
        yield {'tasks': tasks}
        yield {'finishedLoadingPHIDs': [...projColumns.map(column => column.phid), `${project.phid}:finishedLoadingTasks`]}
    }
}

export {
  fetchProjectsColumnsAndTasks,
  fetchPropertiesForUserNames,
  updateTaskOwner
}