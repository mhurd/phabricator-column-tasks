import { ref, watch } from 'vue'
import { useRoute, useRouter } from 'vue-router' 

async function useUrlParameterSync(parameterName, initialValue, valueConverter) {
  const route = useRoute()
  const router = useRouter()
  await router.isReady()
  let value = route.query[parameterName]
  value = (value === undefined) ? initialValue : valueConverter(value)
  const valueRef = ref(value)
  watch(valueRef, (newValue, oldValue) => {
    router.replace({query: {...route.query, [parameterName]: newValue}})
  })
  return { value: valueRef }
}

export async function useUrlStringParameterSync(parameterName, initialValue = '') {
  return await useUrlParameterSync(parameterName, initialValue, value => value || '')
}

export async function useUrlBoolParameterSync(parameterName, initialValue = false) {
  return await useUrlParameterSync(parameterName, initialValue, value => value === 'true')
}