import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'
import { Icon } from '@iconify/vue'

const routes = [
  {
    name: 'app',
    component: App 
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

createApp(App)
  .use(router)
  .component('Icon', Icon)
  .mount('#app')
