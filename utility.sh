#!/bin/bash

get_response_code () {
   echo $(curl --write-out '%{http_code}' --silent --output /dev/null $1);
}

wait_until_url_available () {
   while ! [[ "$(get_response_code $1)" =~ ^(200|301)$ ]]; do sleep 1; done;
   sleep 1;
}

open_urls_in_sandboxed_chrome () {
   rm -r /tmp/phabricator-column-tasks || true;
   open -a "Google Chrome" --args -app "$@" -user-data-dir=/tmp/phabricator-column-tasks -no-default-browser-check -no-first-run -disable-fre;
}

"$@"
