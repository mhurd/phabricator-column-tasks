const express = require('express')
const fetchers = require('./fetchers');

const app = express()

const port = process.env.PORT
const url = process.env.PHABRICATOR_URL
const token = process.env.PHABRICATOR_TOKEN

app.use(express.json())
app.use(express.urlencoded())

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*') // probably restrict this later?
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST') // 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
    res.setHeader('Access-Control-Allow-Credentials', true)
    next()
})

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
})

// curl 127.0.0.1:3000/is-ready
app.get('/is-ready', async (req, res) => {
    res.json({'ready': true})
})

// curl --silent -d '{"userNames": ["mhurd", "zeljkofilipin"]}' -H "Content-Type: application/json" -X POST 127.0.0.1:3000/username-properties | python3 -m json.tool
app.post('/username-properties', async (req, res) => {
    res.send(await fetchers.fetchPropertiesForUserNames(url, token, req.body.userNames))
})

// curl --silent -d '{"phids": ["PHID-PROJ-4623b3ohfuknkil7xf5n", "PHID-PROJ-byp2qkwamzo3txk43bru"]}' -H "Content-Type: application/json" -X POST 127.0.0.1:3000/project-columns | python3 -m json.tool
app.post('/project-columns', async (req, res) => {
    res.send(await fetchers.fetchColumnsForProjectPHIDs(url, token, req.body.phids))
})

// curl --silent -d '{"ids": [6074, 4876, 2660]}' -H "Content-Type: application/json" -X POST 127.0.0.1:3000/projects | python3 -m json.tool
app.post('/projects', async (req, res) => {
    res.send(await fetchers.fetchProjectsForProjectIDs(url, token, req.body.ids))
})

// curl --silent -d '{"userPHID": "PHID-USER-ohzzl3maortma7y4znpb", "taskID": 325110}' -H "Content-Type: application/json" -X POST 127.0.0.1:3000/task-assign | python3 -m json.tool
// curl --silent -d '{"userPHID": null, "taskID": 325110}' -H "Content-Type: application/json" -X POST 127.0.0.1:3000/task-assign | python3 -m json.tool
app.post('/task-assign', async (req, res) => {
    res.send(await fetchers.updateTaskOwner(url, token, req.body.userPHID || '', req.body.taskID))
})

// curl --silent -d '{"phids": ["PHID-PCOL-ffqlvs2tl3uxzj3webbw", "PHID-PROJ-bcf7kqzk5ehd36acxu6k"]}' -H "Content-Type: application/json" -X POST 127.0.0.1:3000/column-and-proxy-tasks | python3 -m json.tool
app.post('/column-and-proxy-tasks', async (req, res) => {
    res.send(await fetchers.fetchTasksForColumnAndProxyPHIDs(url, token, req.body.phids))
})
