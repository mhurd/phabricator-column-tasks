const encodeToURLParameters = (json) => new URLSearchParams(json).toString()

const postOptions = () => {
    return {
        'method': 'POST',
        'Content-Type': 'application/json; charset=utf-8', 
        'Accept': 'application/json'   
    }
}

// indexedArgs('phids', ['a', 'b', 'c']) will output {phids[0]: "a", phids[1]: "b", phids[2]: "c"} 
const indexedArgs = (property, values) => {
    return values.reduce((obj, item, index) => {
        return {...obj, ...{ [`${property}[${index}]`]: item}}
    }, {})
}

const pagingConduitFetch = async (url, body) => {
    let cursor, output
    try {
        do {
            if (cursor && cursor.after) {
                body.after = cursor.after
            }
            const response = await fetch(url, {
                ...postOptions(),
                body: encodeToURLParameters(body)
            })
            const json = await response.json()
            if (json.result.data) {
                output = [...(output || []), ...json.result.data]
            } else {
                output = {...(output || {}), ...json.result}
            }
            cursor = json.result.cursor
        } while (cursor && cursor.after)
    } catch (error) {
        // TODO: retry once on error?
        console.log(error)
    }
    return output
}

const fetchPropertiesForPHIDs = async (url, token, phids) => {
    if (phids.length == 0) {
        return {}
    }
    return await pagingConduitFetch(
        `${url}/api/phid.query`,
        {'api.token': token, ...indexedArgs('phids', phids)}
    )
}

const flatten = arrayOfArrays => [].concat.apply([], arrayOfArrays)

const columnPHIDsFromTaskAttachment = task => {
    const columns = Object.values(task.attachments.columns.boards)
        .map(c => c.columns)
    return flatten(columns).map(c => c.phid)
}

const fetchTasksForColumnPHIDs = async (url, token, phids) => {
    return await pagingConduitFetch(`${url}/api/maniphest.search`, {
        'api.token': token,
        ...scopedIndexedArgs('constraints', 'columnPHIDs', phids),
        'constraints[statuses][0]': 'open',
        'constraints[statuses][1]': 'progress',
        'constraints[statuses][2]': 'stalled',
        'attachments[columns]': 'true',
    })
}

const getTasksByColumnPHID = (tasks, columnPHIDs) => {
    return tasks.reduce((acc, task) => {
        columnPHIDsFromTaskAttachment(task)
            .filter(columnPHID => columnPHIDs.includes(columnPHID))
            .forEach(columnPHID => {
                if (acc[columnPHID] == undefined) {
                    acc[columnPHID] = [task]     
                } else {
                   acc[columnPHID].push(task) 
                }
            })
        return acc
    }, {})
}

const getCleanedOwner = (phid, properties) => filterObjectByKeys(properties[phid], ['name', 'fullName', 'uri', 'phid'])

const getCleanedTask = (task, properties) => {
    return {
        'id': task.id,
        'phid': task.phid,
        'name': task.fields.name,
        'uri': properties[task.phid].uri,
        'owner': task.fields.ownerPHID ? getCleanedOwner(task.fields.ownerPHID, properties) : null,
        'visible': false
    }
}


const fetchTasksForColumnAndProxyPHIDs = async(url, token, phids) => {
    const columnPHIDs = phids.filter(phid => phid.startsWith('PHID-PCOL-'))
    const proxyPHIDs = phids.filter(phid => phid.startsWith('PHID-PROJ-'))

    const columnTasksFetcher = fetchTasksForColumnPHIDs(url, token, columnPHIDs)
    const proxyTaskFetchers = proxyPHIDs.map(projectPHID => fetchTasksForProjectPHID(url, token, projectPHID)) // unfortunately phab api has a bug where it won't return tasks for multiple projects at once
    const allProxyTaskFetchers = Promise.all(proxyTaskFetchers) 
    
    const columnTasks = await columnTasksFetcher
    const arrayOfProxyTaskArrays = await allProxyTaskFetchers

    const tasksByColumnPHID = getTasksByColumnPHID(columnTasks, columnPHIDs)

    const tasksByProxyPHID = proxyPHIDs.reduce((obj, key, index) => {
      obj[key] = arrayOfProxyTaskArrays[index]
      return obj
    }, {})

    const allTasksByColumnAndProxyPHID = {...tasksByColumnPHID, ...tasksByProxyPHID}

    const allTasks = flatten(Object.values(allTasksByColumnAndProxyPHID))
    const allTaskPHIDs = [...new Set(allTasks.map(task => task.phid))]
    const allUserPHIDs = [...new Set(allTasks.map(task => task.fields.ownerPHID).filter(phid => phid !== null))]
    const properties = await fetchPropertiesForPHIDs(url, token, [...allTaskPHIDs, ...allUserPHIDs])

    const cleanedTasksByColumnAndProxyPHID = {}
    const keys = Object.keys(allTasksByColumnAndProxyPHID)
    const values = Object.values(allTasksByColumnAndProxyPHID)

    keys.forEach((key, i) => cleanedTasksByColumnAndProxyPHID[key] = values[i].map(task => getCleanedTask(task, properties)))

    return cleanedTasksByColumnAndProxyPHID
}

// reminder: the phab api has a bug where it won't return project tasks for more than one project!!
const fetchTasksForProjectPHID = async (url, token, phid) => {
    return await pagingConduitFetch(`${url}/api/maniphest.search`, {
        'api.token': token,
        'constraints[projects][0]': phid,
        'constraints[statuses][0]': 'open',
        'constraints[statuses][1]': 'progress',
        'constraints[statuses][2]': 'stalled',
    })
}

const updateTaskOwner = async (url, token, userPHID, taskID) => {
    const options = {
        ...postOptions(),
        body: encodeToURLParameters({
            'api.token': token,
            'transactions[0][type]': 'owner',
            'transactions[0][value]': userPHID,
            'objectIdentifier': `T${taskID}`,
            'output': 'json'
        })
    }
    const response = await fetch(`${url}/api/maniphest.edit`, options)
    return await response.json()
}

// scopedIndexedArgs('constraints', 'assigned', ['a', 'b', 'c']) will output {constraints[assigned][0]: "a", constraints[assigned][1]: "b", constraints[assigned][2]: "c"} 
const scopedIndexedArgs = (scope, property, values) => {
    return values.reduce((obj, item, index) => {
        return {...obj, ...{ [`${scope}[${property}][${index}]`]: item}}
    }, {})
}

const getCleanedProject = (project, properties) => {
    return {
        'id': project.id,
        'phid': project.phid,
        'name': project.fields.name,
        'fullName': properties[project.phid].fullName,
        'uri': properties[project.phid].uri,
        'columns': [],
        'visible': true
    }
}

const fetchProjectsForProjectIDs = async (url, token, projectIds) => {
    if (projectIds.length == 0) {
        return {}
    }
    const projects = (await pagingConduitFetch(`${url}/api/project.search`, {
        'api.token': token,
        ...scopedIndexedArgs('constraints', 'ids', projectIds)
    }))
    const phids = projects.map(project => project.phid)
    const properties = await fetchPropertiesForPHIDs(url, token, phids)
    return projects.map(project => getCleanedProject(project, properties))
}

const getCleanedColumn = (column, properties) => {
    return {
        'phid': column.phid,
        'name': column.fields.name,
        'proxyPHID': column.fields.proxyPHID,
        'uri': properties[column.phid].uri,
        'tasks': [],
        'visible': false
    }
}

const fetchColumnsForProjectPHIDs = async (url, token, phids) => {
    let columns = (await pagingConduitFetch(`${url}/api/project.column.search`, {
        'api.token': token,
        ...scopedIndexedArgs('constraints', 'projects', phids),
        ...scopedIndexedArgs('constraints', 'statuses', ['0'])
    }))
    .sort((a, b) => {
        // sort by project then by sequence
        if (a.fields.project.id === b.fields.project.id){
            return parseInt(a.fields.sequence) - parseInt(b.fields.sequence)
        } else {
            return a.fields.project.id - b.fields.project.id
        }
    })
    const columnPHIDs = columns.map(column => column.phid)
    // open/closed status only seems to be available in phid.query results - only way to know if column is hidden?
    const properties = await fetchPropertiesForPHIDs(url, token, columnPHIDs)
    columns = columns.filter(column => { return properties[column.phid].status == "open" })

    return columns.reduce((acc, column) => {
        if (acc[column.fields.project.phid] === undefined) {
            acc = {...acc, [column.fields.project.phid] : []} 
        }
        acc[column.fields.project.phid].push(getCleanedColumn(column, properties))
        return acc
    }, {})
}

const filterObjectByKeys = (object, keys) => Object.fromEntries(Object.entries(object).filter(([key]) => keys.includes(key)))

const fetchPropertiesForUserNames = async (url, token, userNames) => {
    let users = (await pagingConduitFetch(`${url}/api/user.search`, {
        'api.token': token,
        ...scopedIndexedArgs('constraints', 'usernames', userNames)
    }))
    return Object.values(await fetchPropertiesForPHIDs(url, token, users.map(user => user.phid)))
        .map(userProps => filterObjectByKeys(userProps, ['name', 'fullName', 'uri', 'phid']))
}

module.exports = {
    fetchPropertiesForUserNames,
    fetchTasksForColumnAndProxyPHIDs,
    updateTaskOwner,
    fetchProjectsForProjectIDs, 
    fetchColumnsForProjectPHIDs
}