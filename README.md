![screenshot](screenshot.png "screenshot")

### Features

- Shows list of Phabricator projects and their columns and tasks
- Apply instant filters on column titles, task titles, task numbers and task assignees
- Allows task assignees to be quicky changed
- Settings are synced to the url so your configuration is bookmarkable

### To use

- Ensure you have [Docker](https://www.docker.com/products/docker-desktop/) installed
- Specify Phabricator [url](https://gitlab.wikimedia.org/mhurd/phabricator-column-tasks/-/blob/main/docker-compose.yml#L16) and Conduit [token](https://gitlab.wikimedia.org/mhurd/phabricator-column-tasks/-/blob/main/docker-compose.yml#L15) in [docker-compose.yml](docker-compose.yml)
- Run "./fresh_install" in the phabricator-column-tasks directory

### Notes

The token is used to request data from Phabricator and also when changing task assigned users. Get a token via a link formatted like the following: 

https://YOUR_PHABRICATOR_URL/settings/user/YOUR_PHABRICATOR_USER_NAME/page/apitokens/